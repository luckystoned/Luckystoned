var t = 0;
		var c = document.querySelector("#fireworks");	 
		var $ = c.getContext('2d');
		c.width = window.innerWidth;
		c.height = window.innerHeight;
		$.fillStyle = 'hsla(0,0%,0%,1)';

		window.addEventListener('resize', function() {
		  c.width = window.innerWidth;
		  c.height = window.innerHeight;
		}, false);
		var l = Math.floor((Math.random() * 10) + 1);
		function draw() {
		  $.globalCompositeOperation = 'source-over';
		  $.fillStyle = 'hsla(0,0%,0%,.1)';
		  $.fillRect(0, 0, c.width, c.height);
		  var foo, i, j, r;
		  foo = Math.sin(t) * 1 * Math.PI;
		  for (i=0; i<600; ++i) {
			r = 600 * Math.sin(i * foo);
			$.globalCompositeOperation = '';
			$.fillStyle = '#717171';
			$.beginPath();
			$.arc(Math.sin(i) * r + (c.width / 2), 
				  Math.cos(i) * r + (c.height / 2), 
				  1.5, 0, Math.PI * 2);
			$.fill();

		  }
		  t += 10;
		  return t %= 10 * Math.PI;

		};

		function run() {
		  window.requestAnimationFrame(run);
		  draw();
		}
		run();